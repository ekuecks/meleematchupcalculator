# Melee Matchup Calculator

## Usage
Enter the values for each individual stage for the matchup in matchups.txt from 
your perspective. Ex: If you are fox against marth and you believe that fox has 
a 40% chance to win on Final Destination, put a 0.4 after "Final:" in matchups.txt. After filling out the matchups, run `python script.py`. The script will give you the matchup ratio from your perspective for best of 3 and best of 5.
