def recurse(w, l, nxt, stages, counter, starter):
    # You won
    if w == 3:
        return 1
    # You lost
    elif l == 3:
        return 0
    # Stagelist after we win
    stagesw = []
    # Stagelist after we lose
    stagesl = []
    # This is the first game
    if starter:
        # Starter stage will never be picked again so
        # Don't need to change stages
        stagesw = stages
        stagesl = stages
    # This is our counterpick
    elif counter:
        # We are playing on our best stage (stages[0])
        # If we win we can no longer go back here
        stagesw = stages[1:]
        stagesl = stages
    # This is our opponent's counterpick
    else:
        # We are playing on our worst stage (stages[-1])
        # If we lose we can no longer be taken here
        stagesw = stages
        stagesl = stages[0:-1]
    return (
        nxt*recurse(w+1, l, stages[-1], stagesw, False, False)
        + (1-nxt)*recurse(w, l+1, stages[0], stagesl, True, False)
    )

def bo_five(starter, stages):
    return recurse(0, 0, starter, stages, False, True)

def bo_three(starter, stages):
    # Best and worst stages will be banned
    return recurse(1, 1, starter, stages[1:-1], False, True)

def calculate(starter, stages):
    return bo_three(starter, stages), bo_five(starter, stages)

def main():
    matchups = {}
    with open('matchups.txt', 'r') as f:
        for line in f.readlines():
            line = line.strip()
            line = line.split(':')
            matchups[line[0]] = float(line[1])
    # 5 starter stages
    stages = [
        ratio
        for stage, ratio
        in matchups.items()
        if stage != 'Pokemon'
    ]
    # sort by matchup ratio low to high
    stages = sorted(stages)
    # most even starter stage to start
    starter = stages[2]
    # add in Pokemon to the stages
    stages.append(matchups['Pokemon'])
    # re-sort stages
    stages = sorted(stages)

    three, five = calculate(starter, stages)
    print('Best of 3: {}\nBest of 5: {}'.format(three, five))

if __name__ == "__main__":
    main()
